﻿using System.Collections.Generic;
using System.Collections;
using System.IO.Ports;
using UnityEngine;

public class HandController : MonoBehaviour
{
    SerialPort serialPort = new SerialPort("COM3", 9600);
    ThumbController thumbController;
    IndexController indexController;
    MiddleController middleController;
    RingController ringController;
    PinkyController pinkyController;
    public GameObject HandMotion;
    public Vector3  HandV; 
    public float sensitivity;
    private float posX;
    private float posY;
    private float posZ;   
    void Start()
    {
        serialPort.Open();
        serialPort.ReadTimeout = 100;
        //Hand Movement Vector
        HandV = HandMotion.transform.eulerAngles;
        //Object Controller at Scripts on Unity Project
        thumbController = FindObjectOfType<ThumbController>();
        indexController = FindObjectOfType<IndexController>();
        middleController = FindObjectOfType<MiddleController>();
        ringController = FindObjectOfType<RingController>();
        pinkyController = FindObjectOfType<PinkyController>();
        //Initial Position of every Finger
        thumbController.InitialPosition();
        indexController.InitialPosition();
        middleController.InitialPosition();
        ringController.InitialPosition();
        pinkyController.InitialPosition();
    }
    void Update()
    {
        try
        {
            sensitivity = 0.9f;
            if(!serialPort.IsOpen){
                Debug.Log("Forzando Apertura");
                serialPort.Open();
            }
            string [] SplitData = serialPort.ReadLine().Split(',');

            posX += float.Parse(SplitData [0])/1000;
            posY += float.Parse(SplitData [1])/1000;
            posZ += float.Parse(SplitData [2])/1000;

            posX = posX * sensitivity;
            posY = posY * sensitivity;
            posZ = posZ * sensitivity;

            UpdatePosition_Anchor(posX, posY, posZ);
            thumbController.UpdatePosition_Thumb1(int.Parse(SplitData[6])/10, 0, int.Parse(SplitData[7])/10);
            indexController.UpdatePosition_Index1( int.Parse(SplitData[6])/10, 0, int.Parse(SplitData[7])/10);
            middleController.UpdatePosition_Middle1( int.Parse(SplitData[6])/10, 0, int.Parse(SplitData[7])/10);
            ringController.UpdatePosition_Ring1(int.Parse(SplitData[6])/10, 0, int.Parse(SplitData[7])/10);
            pinkyController.UpdatePosition_Pinky1( int.Parse(SplitData[6])/10, 0, int.Parse(SplitData[7])/10);
        }catch(System.Exception err){
          err = new System.Exception();
        }
    }
    void OnApplicationQuit()
    {
        serialPort.Close();
    }
    public void UpdatePosition_Anchor(float x, float y, float z)
    {
        print("Starting Update Position Hand 1V");
        HandMotion.transform.rotation = Quaternion.Euler(HandV.x+x,HandV.y-y,HandV.z+z-100);
    } 
}
