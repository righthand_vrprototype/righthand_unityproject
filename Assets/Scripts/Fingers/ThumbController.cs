﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThumbController : MonoBehaviour
{
    public GameObject ThumbMeta, Thumb1, Thumb2;
    public Vector3  Thumb1V, Thumb2V;
    //Vector 3 (x, y ,z) 
    public void InitialPosition()
    {
        Thumb1V = Thumb1.transform.eulerAngles;
        Thumb2V = Thumb2.transform.eulerAngles;
    }
    public void UpdatePosition_Thumb1(int x, int y, int z)
    {
        print("Starting Update Position Thumb 1V");
        Thumb1.transform.eulerAngles = new Vector3 (Thumb1V.x+x, Thumb1V.y+y, Thumb1V.z+z);
    }
    public void UpdatePosition_Thumb2(int x, int y, int z)
    {
        print("Starting Update Position Thumb 2V");
        Thumb1.transform.eulerAngles = new Vector3 (Thumb2V.x+x, Thumb2V.y+y, Thumb2V.z+z);
    }
}
