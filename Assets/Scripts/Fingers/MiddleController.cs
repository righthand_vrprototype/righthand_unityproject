﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiddleController : MonoBehaviour
{
    public GameObject MiddleMeta, Middle1, Middle2, Middle3;
    // Start is called before the first frame update
    public Vector3  Middle1V, Middle2V, Middle3V;
    public void InitialPosition()
    {
        Middle1V = Middle1.transform.eulerAngles;
        Middle2V = Middle2.transform.eulerAngles;
        Middle3V = Middle3.transform.eulerAngles;
    }
    public void UpdatePosition_Middle1(int x, int y, int z)
    {
        print("Starting Update Position Middle 1V");
        Middle1.transform.eulerAngles = new Vector3 (Middle1V.x+x, Middle1V.y+y, Middle1V.z+z);
    }
    public void UpdatePosition_Middle2(int x, int y, int z)
    {
        print("Starting Update Position Middle 2V");
        Middle1.transform.eulerAngles = new Vector3 (Middle2V.x+x, Middle2V.y+y, Middle2V.z+z);
    }
}
