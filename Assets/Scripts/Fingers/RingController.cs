﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingController : MonoBehaviour
{
    public GameObject RingMeta, Ring1, Ring2, Ring3;
    // Start is called before the first frame update
    public Vector3  Ring1V, Ring2V, Ring3V;
    public void InitialPosition()
    {
        Ring1V = Ring1.transform.eulerAngles;
        Ring2V = Ring2.transform.eulerAngles;
        Ring3V = Ring3.transform.eulerAngles;
    }
    public void UpdatePosition_Ring1(int x, int y, int z)
    {
        print("Starting Update Position Ring 1V");
        Ring1.transform.eulerAngles = new Vector3 (Ring1V.x+x, Ring1V.y+y, Ring1V.z+z);
    }
    public void UpdatePosition_Ring2(int x, int y, int z)
    {
        print("Starting Update Position Ring 2V");
        Ring2.transform.eulerAngles = new Vector3 (Ring2V.x+x, Ring2V.y+y, Ring2V.z+z);
    }
}
