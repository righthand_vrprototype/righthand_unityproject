﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndexController : MonoBehaviour
{
    public GameObject IndexMeta, Index1, Index2, Index3;
    // Start is called before the first frame update
    public Vector3  Index1V, Index2V, Index3V;
    public void InitialPosition()
    {
        Index1V = Index1.transform.eulerAngles;
        Index2V = Index2.transform.eulerAngles;
        Index3V = Index3.transform.eulerAngles;
    }
    public void UpdatePosition_Index1(int x, int y, int z)
    {
        print("Starting Update Position Index 1V");
        Index1.transform.eulerAngles = new Vector3 (Index1V.x+x, Index1V.y+y, Index1V.z+z);
    }
    public void UpdatePosition_Index2(int x, int y, int z)
    {
        print("Starting Update Position Index 2V");
        Index1.transform.eulerAngles = new Vector3 (Index2V.x+x, Index2V.y+y, Index2V.z+z);
    }
}
