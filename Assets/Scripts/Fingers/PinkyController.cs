﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyController : MonoBehaviour
{
    public GameObject PinkyMeta, Pinky1, Pinky2, Pinky3;
    // Start is called before the first frame update
    public Vector3  Pinky1V, Pinky2V, Pinky3V;
    public void InitialPosition()
    {
        Pinky1V = Pinky1.transform.eulerAngles;
        Pinky2V = Pinky2.transform.eulerAngles;
        Pinky3V = Pinky3.transform.eulerAngles;
    }
    public void UpdatePosition_Pinky1(int x, int y, int z)
    {
        print("Starting Update Position Pinky 1V");
        Pinky1.transform.eulerAngles = new Vector3 (Pinky1V.x+x, Pinky1V.y+y, Pinky1V.z+z);
    }
    public void UpdatePosition_Pinky2(int x, int y, int z)
    {
        print("Starting Update Position Pinky 2V");
        Pinky1.transform.eulerAngles = new Vector3 (Pinky2V.x+x, Pinky2V.y+y, Pinky2V.z+z);
    }
}
